/* 
 * Southern NH University
 * IT - 145 Foundations in Application Development
 * Instructor: Joe Parker
 * Student: Shawn Doherty 
 * Date: 06/18/19
 * 
 * Description: Zoo Monitor
 * 
 * This application monitors activites and the living habitats of the animals
 * in the zoo.  The zookeeper may choose to monitor an animal a habitat or exit 
 * the application from the main menu.  The application will list the available
 * animals or habitats for the zookeeper to select.  When an item is selected, 
 * the zookeeper will see the monitoring information and be presented with a 
 * notification window popup if there are any out of range conditions.  
 */
package monitor;

/**
 *
 * @author sdoherty
 */
import java.util.Scanner;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Monitor {

    /**
     * This is the main class of the monitoring application. This class provides
     * the decision making framework for the program by providing the main menu
     * and list menu as well as accepting user input. This class also handles
     * the output and display
     * @return
     */
    public static char mainMenuSelection() {
        /**
         * Returns a char containing the main application selection. Asks a user
         * to monitor an animal, monitor a habitat, or exit. Return A for
         * animal, H for habitat or Q for quit
         */

        //create a scanner object for recieving input from user
        Scanner scnr = new Scanner(System.in);
        //prompt user for input
        String prompt = "What would you like to monitor? \n Enter A for Animal,"
                + " H for Habitat, or Q to Quit.";
        System.out.println(prompt);
        //use input from prompt as a return variable.
        char mainSelection = Character.toUpperCase(scnr.next().charAt(0));
        //INPUT VALIDATION: prompt until a valid character is entered.
        while (mainSelection != 'Q' && mainSelection != 'A' && mainSelection != 'H') {
            System.out.println(prompt);
            mainSelection = Character.toUpperCase(scnr.next().charAt(0));
        }
        //return the main menu selection (either A, H, or Q)
        return mainSelection;
    }

    /**
     *
     * @param entries
     * @param listCount
     * @return
     * @throws IOException
     */
    public static char listMenuSelection(String entries, int listCount) throws IOException {
        /**
         * Given a list of options get a selection from the user in the form of
         * a char. After user is presented with list of numeric options to
         * monitor, prompt for input to select an option or go back to main
         * menu. Valid return char are [b,B] or numeric values from 1 - a
         * counter value scanned at the first line of the entries string.
         */

        //create a scanner object of the data.
        Scanner dataReader = new Scanner(entries);
        //first line of data set is counter. String converted to num val of char
        int counter = listCount;
        //create scanner object to recieve input from user
        Scanner scnr = new Scanner(System.in);
        //prompt for input
        String numericPrompt = "Please enter a number(1 - " + (counter) + ") or Enter \"B\" to return to previous menu.";
        System.out.print(numericPrompt);

        //input validation: take only the first char of any input
        char listSelection = scnr.next().charAt(0);
        //re-prompt if the input not in correct range or does not match [b,B]
        while (Character.toUpperCase(listSelection) != 'B' && (Character.getNumericValue(listSelection) < 1 || Character.getNumericValue(listSelection) > counter)) {
            System.out.print(numericPrompt);
            listSelection = scnr.next().charAt(0);
        }
        //return char, example1: b example2: 1
        return listSelection;
    }

    public static String getSearchTerm(String list, char selectionChar) throws IOException {
        /**
         * Function to return a string that will be used as the search pattern
         * in the data set. Given ARG1(a list) and ARG2 (a numeric selection as
         * a char). Iterate through the list until the correct selection is
         * reached and return the line as a string. Example: Lions.
         */

        //create a scanner object from the list
        Scanner listReader = new Scanner(list);
        //initialize variable for return
        String monitorSelection = "";

        // ignore the first entry in the list.
        // assign a temp value to get past the first line. 
        //String tmpCount = listReader.nextLine();
        //This value is the line number that holds the selection text
        int counter = Character.getNumericValue(selectionChar);
        //System.out.println("counter from get entry string is: "+ counter);
        //cycle through next line values to get the correct line. 
        for (int i = 0; i < counter; i++) {
            //assign the line content to the return variable
            monitorSelection = listReader.nextLine();
        }
        //return example1:Lions, example2:Bird house
        //System.out.println("you have chosen: "+ monitorSelection);
        return monitorSelection;
    }

    public static void displayInfo(String userSelection, String dataSet) throws IOException {
        /**
         * Matches and displays based on search criteria. ARG1 is the selection
         * to match, ARG2 is the data to search.
         */

        //assign the search term to variable lookFor
        String lookFor = userSelection;

        //STRING FORMATTING
        //HABITAT: some entries contain a last word that needs to be removed
        if (lookFor.contains(" ")) {
            lookFor = lookFor.substring(0, lookFor.lastIndexOf(" "));
        }

        //ANIMAL: remove plurals from animal names
        if (lookFor.charAt((lookFor.length() - 1)) == 's') {
            lookFor = lookFor.substring(0, lookFor.length() - 1);
        }

        //setup scanner for data
        Scanner readData = new Scanner(dataSet);

        //initialize empty string for details section
        while (readData.hasNext()) {
            String line = readData.nextLine();
            //set the alert message to null for now and popup after console output
            String alertMessage = null;
            //if a match is found, print to console
            if (line.contains(lookFor)) {
                String header = line;
                System.out.println("\n" + line);
                //the next 3-4 lines are the details that we want to display
                int i = 0;
                //animal entries contain 1 more field than habitat
                if (header.contains("Habitat")) {
                    i = 1;
                }
                while (i < 4) {
                    //assign a new line variable for out of range verification
                    String newLine = readData.nextLine();
                    //check for out of range if line contains a match
                    if (newLine.contains("*****")) {
                        //strip asterisk and create alert text
                        alertMessage = newLine.substring(5);

                        //display to console 
                        System.out.println(alertMessage);
                    } else {
                        System.out.println(newLine);
                    }
                    ++i;
                }

                //OUT OF RANGE NOTIFICATION
                if (alertMessage != null) {
                    JOptionPane.showMessageDialog(null, alertMessage, "ALERT!", JOptionPane.INFORMATION_MESSAGE);
                }
                System.out.println("");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        /**
         * The Main method addresses the branches of the application and directs
         * the output from the helper methods. This method is responsible for
         * assigning the correct data files to the data getter and display
         * structures.
         */

        //Initial Greeting
        System.out.println("Zoo Monitoring System: Shawn Doherty");
        //init String variable to hold custom data set.
        String monitorFile = "";
        //return the user's main menu decision((A)nimal, (H)abitat, or (Q)uit)
        char userLetter = mainMenuSelection();

        //keep the application alive as long as the user does not choose to quit
        //while the user wishes to keep using the monitoring system. 
        while (userLetter != 'Q') {

            //ANIMAL BRANCH
            if (userLetter == 'A') {
                //use animals.txt as a data source for the animal branch
                monitorFile = "animals.txt";
                //display animal specific input request
                System.out.println("\nPlease select from the following animal monitoring choices:");

            } //HABITAT BRANCH
            else if (userLetter == 'H') {
                //use habitats.txt as a data source for the habitat branch
                monitorFile = "habitats.txt";
                //display habitat specific input request
                System.out.println("\nPlease select from the following habitat monitoring choices:");
            }

            //to access the data objects create a new instance named monitorItem
            AnimalsHabitat monitorSubject = new AnimalsHabitat();
            //use the correct text file to create a data set.
            monitorSubject.setData(monitorFile);

            //create a variable to hold the users selection from the list of 
            // possible monitoring options(either a number or "B" to go back
            char listSelectionChar = listMenuSelection(monitorSubject.getList(), monitorSubject.getCounter());

            //if user does not choose to go back pass the selection through
            if (Character.toUpperCase(listSelectionChar) != 'B') {
                //create a string to based on the users input that will hold the habitat
                //or animal that has been chosen
                String choice = getSearchTerm(monitorSubject.getList(), listSelectionChar);
                //pass the string to search and correct data to the display function
                displayInfo(choice, monitorSubject.getDetails());
            }
            //return to the main menu
            userLetter = mainMenuSelection();
        }
    }
}
