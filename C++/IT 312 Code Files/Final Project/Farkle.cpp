/* 
Shawn Doherty 
IT-312-J1006 Software Devel w/C++.Net 19EW1 
date
Final Project:Farkle
*/

/*
Develop a program that follows the rules of Farkle as described. On program start-up, it shall display the
rules to the user as read from a text file submitted with the program. The user can then set up the game
by entering the number of players. Any number below two shall ask the user to add more players. Once
gameplay has started based on the game rules, there are a few main pieces to address. Rolling the die
should be performed by randomly generating the side of the die displayed for each of the six using a
random number generator. After each player’s roll, calculate the score for the roll based on the user’s
selection for scoring. Validate that the scoring option applies to the dice and determine if a subsequent
role is allowed. Continue playing until a player reaches 10,000 points. Display a message to the game
winner.
Farkle is a dice game that is multi-player with a minimum of two players, but no upper limit on the
number of participants. The goal is to reach 10,000 points first.
*/

#include <iostream>
#include <cstdlib>
//include time library for random number seeding
#include <ctime>
using namespace std;

//declare array for dice roll values
int diceRoll[6];

int main()
{
    int totalPoints = 0;
    //start with 6 dice
    int diceInPlay = 6;
    srand(time(NULL));
    //TODO: greeting should be separate read file.
    cout << "THE RULES ARE: THERE ARE NO RULES!" << endl;
    
    //DICE ROLL
    //roll all 6 dice
    for (int a = 0; a < diceInPlay; a++)
    {
         diceRoll[a]= rand() % 6 + 1;

    }
    
    // print output
    cout << "Your roll ";
    for (int a = 0; a < diceInPlay; a++)
    {
        cout << diceRoll[a] << " " ;
        if (diceRoll[a] == 1){
            totalPoints = totalPoints + 100;
        }
        else if (diceRoll[a] == 5)
        {
            totalPoints = totalPoints + 50;
        }
        
    }
    cout << endl;
    cout << "Points total : " << totalPoints << endl; 

    return 0;
}

