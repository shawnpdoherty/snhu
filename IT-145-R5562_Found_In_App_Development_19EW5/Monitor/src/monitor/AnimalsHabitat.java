/* 
 * Southern NH University
 * IT - 145 Foundations in Application Development
 * Instructor: Joe Parker
 * Student: Shawn Doherty 
 * Date: 06/18/19
 * 
 * Description: Zoo Monitor
 * 
 * This application monitors activites and the living habitats of the animals
 * in the zoo.  The zookeeper may choose to monitor an animal a habitat or exit 
 * the application from the main menu.  The application will list the available
 * animals or habitats for the zookeeper to select.  When an item is selected, 
 * the zookeeper will see the monitoring information and be presented with a 
 * notification window popup if there are any out of range conditions.  
 */
package monitor;

/**
 *
 * @author sdoherty
 */
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

public class AnimalsHabitat {

    /**
     * The purpose of this class is to parse .txt files and create list and
     * detail objects. The set function accepts a specific text file and creates
     * a list of animals or habitats as listEntries, a string of specific
     * monitoring details, and provides a counter of how many objects there are
     * provided by the text file. The associated get methods provide access to
     * the object variable data from outside the class.
     */
    
    //create private class variables
    private String listEntries;
    private String itemDetails;
    private int counter;

    //initialize public instances
    public AnimalsHabitat() {
        listEntries = "none";
        itemDetails = "none";
        counter = 0;
    }

    //setter function
    public void setData(String fileName) throws IOException {
        /**
         * This function handles the initial user selection of either animal or
         * habitat and returns a list of animals or habitats, the number of
         * entries, and the full details as separate data objects. Scan the
         * appropriate file passed in as argument. Return data as list of
         * entries, individual details, and a total counter of entries.
         */

        //Create new FileInputStream object named fileByteStream and init
        FileInputStream fileByteStream;
        //Create new Scanner object named inFS and init
        Scanner inFS;
        //assign object to filename
        fileByteStream = new FileInputStream(fileName);
        //assign scanner to FileInputStream object
        inFS = new Scanner(fileByteStream);

        //create stringwriter and printwriter objects for details list
        StringWriter listCharStream = new StringWriter();
        PrintWriter fullList = new PrintWriter(listCharStream);

        //create objects for data list
        StringWriter dataCharStream = new StringWriter();
        PrintWriter fullData = new PrintWriter(dataCharStream);

        // CREATE LIST
        // init a counter
        counter = 0;
        //read file line by line
        while (inFS.hasNext()) {
            String line = inFS.nextLine();
            //if the line starts with Details on
            if (line.startsWith("Details on")) {
                //add option to list data structure in printwriter
                //example1: Lions, example2:Bird house
                fullList.print(line.substring(11, 12).toUpperCase() + line.substring(12) + "\n");
                //prefix numeric value to the entry and display
                System.out.println((counter + 1) + " - " + line);
                //increment counter for each iteration. 
                counter = counter + 1;
            } else {
                //the rest of the file will be added to the 'data' section of the dataset
                fullData.print(line + "\n");
            }
        }

        //create the list of options from the data file
        listEntries = listCharStream.toString();
        //create the specific details from the data file
        itemDetails = dataCharStream.toString();
        //close the file input stream
        fileByteStream.close();
    }

    //getter functions
    public String getList() {
        return listEntries;
    }

    public String getDetails() {
        return itemDetails;
    }

    public int getCounter() {
        return counter;
    }
}
